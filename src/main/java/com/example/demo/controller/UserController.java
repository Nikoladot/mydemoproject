package com.example.demo.controller;


import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getOne(@PathVariable("id") Long id) {

		User obj = userService.findOne(id).orElse(null);

		if (obj == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}

		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDto = modelMapper.map(obj, UserDTO.class);
		return new ResponseEntity<UserDTO>(userDto, HttpStatus.OK);

	}

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody User user) {
		User newUser = new User();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(user.getPassword());
		newUser = userService.save(newUser);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}
	
	
	@RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id){
		
		Iterable<User> users = userService.findAll();
		
		if(users==null) {
			return new ResponseEntity<Iterable<User>>(HttpStatus.NOT_FOUND);
		}
		
		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDto = modelMapper.map(users, UserDTO.class);
		return new ResponseEntity<UserDTO> (userDto, HttpStatus.OK);
	}
	
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable Long id) {
    	
        Optional<User> user = userService.findOne(id);
        if (user.isPresent()) {
            userService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody User user) {
    	
        Optional<User> tempUser = userService.findOne(id);
        
        if(tempUser.isPresent()) {
        	
        	userService.save(user);
        	
        	 return new ResponseEntity(HttpStatus.OK);
        	
        }
        
        else {
        	
        	return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
        }
        
           
        
    }

    
}

package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> { // or extends CrudRepository<User, Long>
	Optional<User> findByUsername(String username);
}
